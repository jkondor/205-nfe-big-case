package br.com.bigcase.imposto.consumer;

import br.com.bigcase.imposto.service.ImpostoService;
import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ImpostoConsumer {
    @Autowired
    ImpostoService impostoService;

    @KafkaListener(topics = "spec2-joao-augusto-2", groupId = "1")
    public void receber(@Payload NotaFiscalEletronica notaFiscalEletronica) throws IOException {
        impostoService.calcularImposto(notaFiscalEletronica);
    }
}
