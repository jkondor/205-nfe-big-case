package br.com.bigcase.imposto.notafiscal;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import br.com.bigcase.security.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@FeignClient(name = "notafiscal", configuration = OAuth2FeignConfiguration.class)
public interface NotaFiscalFeign {

    @PutMapping("/nfe")
    NotaFiscalEletronica atualizarEmissaoNota(@RequestBody NotaFiscalEletronica notaFiscalEletronica);
}
