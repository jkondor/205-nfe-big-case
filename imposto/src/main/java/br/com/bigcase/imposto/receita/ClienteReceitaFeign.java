package br.com.bigcase.imposto.receita;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", url = "https://www.receitaws.com.br/v1/cnpj/")
public interface ClienteReceitaFeign {

    @GetMapping("/{cnpj}")
    ClienteReceitaDTO getByCNPJ(@PathVariable String cnpj);
}
