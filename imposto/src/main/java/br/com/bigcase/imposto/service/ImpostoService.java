package br.com.bigcase.imposto.service;

import br.com.bigcase.imposto.notafiscal.NotaFiscalFeign;
import br.com.bigcase.imposto.receita.ClienteReceitaDTO;
import br.com.bigcase.imposto.receita.ClienteReceitaFeign;
import br.com.bigcase.notafiscal.NFE.model.Imposto;
import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ImpostoService {

    @Autowired
    NotaFiscalFeign notaFiscalFeign;
    @Autowired
    ClienteReceitaFeign clienteReceitaFeign;

    public void calcularImposto (NotaFiscalEletronica notaFiscalEletronica){

        if (notaFiscalEletronica.getIdentidade().length() ==11){
            //Calculo para CPF
            Imposto imposto = new Imposto();
            imposto.setValorInicial(notaFiscalEletronica.getValor());
            imposto.setValorCofins(BigDecimal.valueOf(0));
            imposto.setValorCSLL(BigDecimal.valueOf(0));
            imposto.setValorIRRF(BigDecimal.valueOf(0));
            imposto.setValorFinal(notaFiscalEletronica.getValor());

            notaFiscalEletronica.setNfe(imposto);
        } else{
            //Para emitir NF Pessoa Jurídica optante pelo simples nacional. Será descontado somente o IRRF (1,5%) R$ 15,00, valor da NF líquido será R$ 985,00.
            //Para Emitir NF Pessoa Jurídica não optante pelo simples nacional. Será  descontado o IRRF (1,5%) , CSLL (3%) , PIS /Cofins (0,65%). Seria R$ 948,50 valor líquido.
            Imposto imposto = new Imposto();
            BigDecimal valorIRRF;
            BigDecimal valorCSLL;
            BigDecimal valorCofins;

            ClienteReceitaDTO clienteReceitaDTO = clienteReceitaFeign.getByCNPJ(notaFiscalEletronica.getIdentidade());

            if (Double.parseDouble(clienteReceitaDTO.getCapital_social()) >1000000.00){
                System.out.println("Capital social MAIOR que 1 milhão");
                imposto.setValorInicial(notaFiscalEletronica.getValor());
                valorIRRF = notaFiscalEletronica.getValor().multiply(new BigDecimal(98.50));
                System.out.println("valorIRRF = " + valorIRRF);
                valorIRRF = valorIRRF.divide(new BigDecimal(100.00));
                valorIRRF = notaFiscalEletronica.getValor().subtract(valorIRRF);
                System.out.println("valorIRRF = " + valorIRRF);
                imposto.setValorIRRF(valorIRRF);

                valorCSLL = notaFiscalEletronica.getValor().multiply(new BigDecimal(97.00));
                System.out.println("valorCSLL =" + valorCSLL);
                valorCSLL = valorCSLL.divide(new BigDecimal(100.00));
                valorCSLL = notaFiscalEletronica.getValor().subtract(valorCSLL);
                System.out.println("valorCSLL =" + valorCSLL);
                imposto.setValorCSLL(valorCSLL);

                valorCofins = notaFiscalEletronica.getValor().multiply(new BigDecimal(99.35));
                System.out.println("valorCofins =" + valorCofins);
                valorCofins = valorCofins.divide(new BigDecimal(100.00));
                valorCofins = notaFiscalEletronica.getValor().subtract(valorCofins);
                System.out.println("valorCofins =" + valorCofins);
                imposto.setValorCofins(valorCofins);
            } else {
                System.out.println("Capital social MENOR que 1 milhão");
                imposto.setValorInicial(notaFiscalEletronica.getValor());
                valorIRRF = notaFiscalEletronica.getValor().multiply(new BigDecimal(98.50));
                System.out.println("valorIRRF = " + valorIRRF);
                valorIRRF = valorIRRF.divide(new BigDecimal(100.00));
                valorIRRF = notaFiscalEletronica.getValor().subtract(valorIRRF);
                System.out.println("valorIRRF = " + valorIRRF);
                imposto.setValorIRRF(valorIRRF);

                valorCSLL = new BigDecimal(0.00);
                imposto.setValorCSLL(valorCSLL);

                valorCofins = new BigDecimal(0.00);
                imposto.setValorCofins(valorCofins);
            }


            BigDecimal valorFinal = notaFiscalEletronica.getValor().subtract(valorIRRF);
            valorFinal = valorFinal.subtract(valorCSLL);
            valorFinal = valorFinal.subtract(valorCofins);
            System.out.println("valorFinal =" + valorFinal);
            imposto.setValorFinal(valorFinal);

            notaFiscalEletronica.setNfe(imposto);
        }

        System.out.println("Depois do calculo imposto");
        System.out.println(notaFiscalEletronica.getId());
        System.out.println(notaFiscalEletronica.getIdentidade());
        System.out.println(notaFiscalEletronica.getValor());
        System.out.println(notaFiscalEletronica.getStatus());
        System.out.println(notaFiscalEletronica.getNfe().getValorInicial());
        System.out.println(notaFiscalEletronica.getNfe().getValorInicial());
        System.out.println(notaFiscalEletronica.getNfe().getValorIRRF());
        System.out.println(notaFiscalEletronica.getNfe().getValorCSLL());
        System.out.println(notaFiscalEletronica.getNfe().getValorCofins());
        System.out.println(notaFiscalEletronica.getNfe().getValorFinal());

        NotaFiscalEletronica notaAtualizada = notaFiscalFeign.atualizarEmissaoNota(notaFiscalEletronica);

    }
}
