package br.com.bigcase.notafiscal.NFE.model.enums;

public enum Status {
    PENDENTE,
    FINALIZADO
}
