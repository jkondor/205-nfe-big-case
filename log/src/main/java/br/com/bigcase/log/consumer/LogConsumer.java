package br.com.bigcase.log.consumer;

import br.com.bigcase.log.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class LogConsumer {
    @Autowired
    LogService logService;

    @KafkaListener(topics = "spec2-joao-augusto-1", groupId = "Log")
    public void receber(@Payload String logNFE) throws IOException {

        System.out.println("Recebi a log: " + logNFE);
        logService.gravarArquivoLog(logNFE);

    }
}
