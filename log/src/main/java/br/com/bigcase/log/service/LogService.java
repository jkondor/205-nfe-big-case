package br.com.bigcase.log.service;

import br.com.bigcase.log.exception.LogNotSaveException;

import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Service
public class LogService {

    public void gravarArquivoLog (String logNFE) throws IOException {

        try {
            FileWriter fw = new FileWriter("LogNFE.csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);

            pw.println(logNFE);

            pw.close();
            
        }catch(Exception E) {
            throw new LogNotSaveException();
        }
    }
}
