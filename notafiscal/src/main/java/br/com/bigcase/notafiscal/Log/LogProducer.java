package br.com.bigcase.notafiscal.Log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

@Service
public class LogProducer {
    @Autowired
    private KafkaTemplate<String, String> producer;

    public void enviarAoKafka(String funcao, String identidade, BigDecimal valor) {
        String logString = new String();

        DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;
        String formattedDate = formatter.format(Instant.now());
//Verificar porque 3 horas a mais
        if (funcao.equals("post")){
            logString = "[" + formattedDate + "][Emissão]:" + identidade + " acaba de pedir a emissão de uma NF no valor de " + valor + "!";
        }else{
            logString = "[" + formattedDate + "][Consulta]:" + identidade + " acaba de pedir os dados das suas notas fiscais.";
        }

        System.out.println(logString);
        producer.send("spec2-joao-augusto-1", logString);
    }
}
