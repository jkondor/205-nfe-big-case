package br.com.bigcase.notafiscal.NFE.controller;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEGetResponseDTO;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEPostRequestDTO;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEPostResponseDTO;
import br.com.bigcase.notafiscal.NFE.model.mapper.NFEMapper;
import br.com.bigcase.notafiscal.security.Usuario;
import br.com.bigcase.notafiscal.NFE.service.NFEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/nfe")
public class NFEController {
    @Autowired
    NFEService NFEService;

    @PostMapping("/emitir")
    @ResponseStatus(HttpStatus.CREATED)
    public NFEPostResponseDTO emitir(@RequestBody NFEPostRequestDTO NFEPostRequestDTO, @AuthenticationPrincipal Usuario usuario){

        NotaFiscalEletronica notaFiscalEletronica = NFEMapper.fromPostRequest(NFEPostRequestDTO);
        NotaFiscalEletronica notaEmitida =  NFEService.emitirNota(notaFiscalEletronica);

        return NFEMapper.toPostResponse(notaEmitida);
    }

    @GetMapping("/consultar/{identidade}")
    public  List<NFEGetResponseDTO>  consultarNotas(@PathVariable String identidade, @AuthenticationPrincipal Usuario usuario){
        List<NFEGetResponseDTO> nfeGetResponseDTOList = new ArrayList<>();

        Iterable<NotaFiscalEletronica> nfeIterable = NFEService.buscarNotaPorIdentidade(identidade);
        for (NotaFiscalEletronica nota: nfeIterable) {
            nfeGetResponseDTOList.add(NFEMapper.toGetResponse(nota));
        }

        return nfeGetResponseDTOList;
    }

    @PutMapping
    public NotaFiscalEletronica atualizarEmissaoNota(@RequestBody NotaFiscalEletronica notaFiscalEletronica , @AuthenticationPrincipal Usuario usuario){
        return NFEService.atualizarEmisaoNota(notaFiscalEletronica);
    }
}
