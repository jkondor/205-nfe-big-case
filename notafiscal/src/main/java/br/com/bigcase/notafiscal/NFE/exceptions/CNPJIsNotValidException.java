package br.com.bigcase.notafiscal.NFE.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "CNPJ não é valido.")
public class CNPJIsNotValidException extends RuntimeException{
}
