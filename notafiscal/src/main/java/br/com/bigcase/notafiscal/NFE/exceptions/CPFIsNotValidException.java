package br.com.bigcase.notafiscal.NFE.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "CPF não é valido")
public class CPFIsNotValidException extends RuntimeException {
}
