package br.com.bigcase.notafiscal.NFE.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Nota fiscal não encontrada")
public class NotaNotFoundException extends RuntimeException {
}
