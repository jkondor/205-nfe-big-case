package br.com.bigcase.notafiscal.NFE.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class Imposto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(name = "valor_inicial")
    private BigDecimal valorInicial;
    @Column(name = "valor_irrf")
    private BigDecimal valorIRRF;
    @Column(name = "valor_cssl")
    private BigDecimal valorCSLL;
    @Column(name = "valor_cofins")
    private BigDecimal valorCofins;
    @NotNull
    @Column(name = "valor_final")
    private BigDecimal valorFinal;

    public Imposto() {
    }

    public Imposto(Long id, @NotNull BigDecimal valorInicial, BigDecimal valorIRRF, BigDecimal valorCSLL, BigDecimal valorCofins, @NotNull BigDecimal valorFinal) {
        this.id = id;
        this.valorInicial = valorInicial;
        this.valorIRRF = valorIRRF;
        this.valorCSLL = valorCSLL;
        this.valorCofins = valorCofins;
        this.valorFinal = valorFinal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValorInicial() {
        return valorInicial;
    }

    public void setValorInicial(BigDecimal valorInicial) {
        this.valorInicial = valorInicial;
    }

    public BigDecimal getValorIRRF() {
        return valorIRRF;
    }

    public void setValorIRRF(BigDecimal valorIRRF) {
        this.valorIRRF = valorIRRF;
    }

    public BigDecimal getValorCSLL() {
        return valorCSLL;
    }

    public void setValorCSLL(BigDecimal valorCSLL) {
        this.valorCSLL = valorCSLL;
    }

    public BigDecimal getValorCofins() {
        return valorCofins;
    }

    public void setValorCofins(BigDecimal valorCofins) {
        this.valorCofins = valorCofins;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }
}
