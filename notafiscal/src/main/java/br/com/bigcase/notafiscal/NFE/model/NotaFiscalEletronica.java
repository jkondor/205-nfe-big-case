package br.com.bigcase.notafiscal.NFE.model;

import br.com.bigcase.notafiscal.enums.Status;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
public class NotaFiscalEletronica {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String identidade;
    @NotNull
    private BigDecimal valor;
    @NotNull
    private Status status;
    @ManyToOne
    private Imposto nfe;

    public NotaFiscalEletronica() {
    }

    public NotaFiscalEletronica(Long id, @NotNull String identidade, @NotNull BigDecimal valor, @NotNull Status status, Imposto nfe) {
        this.id = id;
        this.identidade = identidade;
        this.valor = valor;
        this.status = status;
        this.nfe = nfe;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Imposto getNfe() {
        return nfe;
    }

    public void setNfe(Imposto nfe) {
        this.nfe = nfe;
    }
}
