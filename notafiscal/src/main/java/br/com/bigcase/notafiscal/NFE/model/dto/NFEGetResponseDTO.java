package br.com.bigcase.notafiscal.NFE.model.dto;

import br.com.bigcase.notafiscal.enums.Status;
import br.com.bigcase.notafiscal.NFE.model.Imposto;

import java.math.BigDecimal;

public class NFEGetResponseDTO {
    private String identidade;
    private BigDecimal valor;
    private Status status;
    private Imposto nfe;
//DecimalFormat df = new DecimalFormat("###,###.00");
    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Imposto getNfe() {
        return nfe;
    }

    public void setNfe(Imposto nfe) {
        this.nfe = nfe;
    }
}
