package br.com.bigcase.notafiscal.NFE.model.dto;

import br.com.bigcase.notafiscal.enums.Status;

public class NFEPostResponseDTO {

    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
