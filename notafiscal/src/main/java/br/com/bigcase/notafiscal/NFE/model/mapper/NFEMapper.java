package br.com.bigcase.notafiscal.NFE.model.mapper;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEGetResponseDTO;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEPostRequestDTO;
import br.com.bigcase.notafiscal.NFE.model.dto.NFEPostResponseDTO;
import br.com.bigcase.notafiscal.NFE.service.ValidaCNPJ;
import br.com.bigcase.notafiscal.NFE.service.ValidaCPF;

public class NFEMapper {

    public static NotaFiscalEletronica fromPostRequest(NFEPostRequestDTO NFEPostRequestDTO){
        NotaFiscalEletronica notaFiscalEletronica = new NotaFiscalEletronica();
        notaFiscalEletronica.setIdentidade(NFEPostRequestDTO.getIdentidade());
        notaFiscalEletronica.setValor(NFEPostRequestDTO.getValor());

        return notaFiscalEletronica;
    }

    public static NFEPostResponseDTO toPostResponse(NotaFiscalEletronica notaFiscalEletronica){
        NFEPostResponseDTO NFEPostResponseDTO = new NFEPostResponseDTO();
        NFEPostResponseDTO.setStatus(notaFiscalEletronica.getStatus());

        return NFEPostResponseDTO;
    }

    public static NFEGetResponseDTO toGetResponse(NotaFiscalEletronica notaFiscalEletronica){
        NFEGetResponseDTO nfeGetResponseDTO = new NFEGetResponseDTO();
        nfeGetResponseDTO.setValor(notaFiscalEletronica.getValor());
        nfeGetResponseDTO.setStatus(notaFiscalEletronica.getStatus());
        nfeGetResponseDTO.setNfe(notaFiscalEletronica.getNfe());

        if (notaFiscalEletronica.getIdentidade().length()==11){
            nfeGetResponseDTO.setIdentidade(ValidaCPF.imprimeCPF(notaFiscalEletronica.getIdentidade()));
        } else {
            nfeGetResponseDTO.setIdentidade(ValidaCNPJ.imprimeCNPJ(notaFiscalEletronica.getIdentidade()));
        }

        //DecimalFormat df = new DecimalFormat("###,###.00");
        return nfeGetResponseDTO;
    }
}
