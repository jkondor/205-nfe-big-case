package br.com.bigcase.notafiscal.NFE.producer;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class NFEProducer {
    @Autowired
    private KafkaTemplate<String, NotaFiscalEletronica> producer;

    public void enviarAoKafka(NotaFiscalEletronica notaFiscalEletronica) {
        producer.send("spec2-joao-augusto-2", notaFiscalEletronica);
    }
}
