package br.com.bigcase.notafiscal.NFE.repository;

import br.com.bigcase.notafiscal.NFE.model.Imposto;
import org.springframework.data.repository.CrudRepository;

public interface ImpostoRepository extends CrudRepository<Imposto,Long> {
}
