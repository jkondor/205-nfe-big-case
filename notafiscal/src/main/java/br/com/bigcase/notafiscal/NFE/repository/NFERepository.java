package br.com.bigcase.notafiscal.NFE.repository;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import org.springframework.data.repository.CrudRepository;

public interface NFERepository extends CrudRepository<NotaFiscalEletronica,Long> {

    Iterable<NotaFiscalEletronica> findByIdentidade(String identidade);
}
