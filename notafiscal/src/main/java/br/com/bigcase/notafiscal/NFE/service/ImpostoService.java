package br.com.bigcase.notafiscal.NFE.service;

import br.com.bigcase.notafiscal.NFE.model.Imposto;
import br.com.bigcase.notafiscal.NFE.repository.ImpostoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImpostoService {

    @Autowired
    ImpostoRepository impostoRepository;
    public Imposto incluirImposto(Imposto imposto){

        return impostoRepository.save(imposto);

    }
}
