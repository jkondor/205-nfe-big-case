package br.com.bigcase.notafiscal.NFE.service;

import br.com.bigcase.notafiscal.Log.LogProducer;
import br.com.bigcase.notafiscal.NFE.exceptions.CNPJIsNotValidException;
import br.com.bigcase.notafiscal.NFE.exceptions.CPFIsNotValidException;
import br.com.bigcase.notafiscal.NFE.exceptions.NotaNotFoundException;
import br.com.bigcase.notafiscal.NFE.model.Imposto;
import br.com.bigcase.notafiscal.NFE.producer.NFEProducer;
import br.com.bigcase.notafiscal.enums.Status;

import br.com.bigcase.notafiscal.NFE.model.NotaFiscalEletronica;
import br.com.bigcase.notafiscal.NFE.repository.NFERepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class NFEService {

    @Autowired
    NFERepository NFERepository;
    @Autowired
    ImpostoService impostoService;
    @Autowired
    LogProducer logProducer;
    @Autowired
    NFEProducer nfeProducer;

    public NotaFiscalEletronica emitirNota(NotaFiscalEletronica notaFiscalEletronica){
        if ((notaFiscalEletronica.getIdentidade().length() == 11)){
            if(ValidaCPF.isCPF(notaFiscalEletronica.getIdentidade())==false){
                throw new CPFIsNotValidException();
            }
        }else{
            if (ValidaCNPJ.isCNPJ(notaFiscalEletronica.getIdentidade())==false){
                throw new CNPJIsNotValidException();
            }
        }

        //regra de negócio
        notaFiscalEletronica.setStatus(Status.PENDENTE);

        //Envia estimulo de gravação de log para tópico do Kafka
        logProducer.enviarAoKafka( "post", notaFiscalEletronica.getIdentidade(), notaFiscalEletronica.getValor());

        NotaFiscalEletronica notaDB =  NFERepository.save(notaFiscalEletronica);
        //Envia estimulo de calculo de imposto para tópico do Kafka
        nfeProducer.enviarAoKafka(notaDB);
        return notaDB;
    }

    public Iterable<NotaFiscalEletronica> buscarNotaPorIdentidade(String identidade){

        logProducer.enviarAoKafka( "get", identidade, BigDecimal.valueOf(0));
        return NFERepository.findByIdentidade(identidade);
    }

    public NotaFiscalEletronica buscarNotaPorID(Long id) {

        Optional<NotaFiscalEletronica> byId = NFERepository.findById(id);

        if (!byId.isPresent()) {
            throw new NotaNotFoundException();
        }
        return byId.get();
    }

    public NotaFiscalEletronica atualizarEmisaoNota(NotaFiscalEletronica notaFiscalEletronica){

        System.out.println("INICIANDO ATUALIZAR IMPOSTO");
        NotaFiscalEletronica notaDB = buscarNotaPorID(notaFiscalEletronica.getId());

        Imposto imposto = impostoService.incluirImposto(notaFiscalEletronica.getNfe());

        notaDB.setStatus(Status.FINALIZADO);
        notaDB.setNfe(imposto);

        return NFERepository.save(notaDB);
    }
}

