package br.com.bigcase.notafiscal.enums;

public enum Status {
    PENDENTE,
    FINALIZADO
}
